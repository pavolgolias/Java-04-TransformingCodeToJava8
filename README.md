## Transforming Code to Java 8

**This example of Java 8 was downloaded from http://agiledeveloper.com/downloads.html**

Exercise 1: Deodorizing Inner-classes:
- Single Abstract Method Interfaces
- Functional Interfaces

Exercise 2: Streamlining Iterations:
- External vs. Internal Iterators
- Specialized functions
- Function composition
- Infinite streams
- Lazy evaluation

Exercise 3: Tell, don't ask:
- Keep your focus on the whole instead of the part
- Code is transparent
- Avoid the smell of null
- Optional

Exercise 4: You Gotta be Kidding:
- Simple tasks that haunt us
- Higher level of abstractions

Exercise 5: Non-intrusive Comparisons:
- Comparable vs. Comparator
- Composition of comparators

Exercise 6: Execute Around Method Pattern:
- Garbage collection
- Resource clean up
- ARM
- Deterministic behavior

Author: Venkat Subramaniam
